const Reminder = require('./../models/reminder.model');
const { scheduleMail } = require('../services/scheduleMail.service')

function createReminder(req,res){
    const { name, email, description, datetime } = req.body;

    const reminder = new Reminder({
        name,
        email,
        description,
        datetime
    });

    reminder.save((err, item) =>{
        if(err){
            res.status(500).json({ message: "A ocurrido un error al momento de crear el recordatorio", err });
        }
        scheduleMail(item);
        res.status(200).json({ message: "Se ha creado el recordatorio con exito!!" });
    });
}


module.exports = {
    createReminder
}