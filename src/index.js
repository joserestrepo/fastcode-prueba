const app = require('./server');
const {port} = require('./config/config');

require('./database');
require('./routers/index');


async function main (){
    try {
        await app.listen(port);
        console.log(`server on port ${port}`);
    } catch (error) {
        console.log("Failed to lift server");
    }
    
}

main();
