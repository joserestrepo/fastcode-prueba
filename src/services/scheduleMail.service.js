const schedule = require('node-schedule');
const { sendMailer } = require('./sendMailer.service');
const Reminder = require('./../models/reminder.model');

async function scheduleMail(reminder){
    const date = new Date(reminder.datetime);
    try {
       const count = await Reminder.countDocuments({datetime: reminder.datetime});
        if(count == 1){
            const j = schedule.scheduleJob(date, async function(datetime){
                try {
                    const reminders = await Reminder.find({datetime})
                    reminders.forEach( async (reminder) =>{
                       await sendMailer(reminder); 
                    })
                    
                } catch (error) {
                    console.log("A ocurrido un error al enviar el mail");
                    console.log(error)
                }
                
            }.bind(null, reminder.datetime));  
        } 
    } catch (error) {
        console.log(error)
    }
    
    
}

module.exports = {
    scheduleMail
}