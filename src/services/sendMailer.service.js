const nodeMailer = require('nodemailer');
const { mail } = require('./../config/config');
function sendMailer(dataReminder){

    let transporter = nodeMailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, 
        auth: {
            user: mail.user,
            pass: mail.pass
        }
    })

    let mailOptions = {
        from: mail.user,
        to: dataReminder.email,
        subject: 'Recordatorio',
        html: `
            <div>
                <h1>
                    Hola, ${dataReminder.name}.
                </h1>
                <p>${dataReminder.description}</p>
            </div>
        `
    }
    
    return transporter.sendMail(mailOptions);
    
}

module.exports = {
    sendMailer
}
