class FormReminder extends HTMLElement {
    constructor() {
        super();
        this.shadow = this.attachShadow({ mode: 'open' });
    }

    initVar(){
        this.form = this.shadow.querySelector('#formReminder');
        this.name = this.shadow.querySelector('#name');
        this.email = this.shadow.querySelector('#email');
        this.description = this.shadow.querySelector('#description');
        this.datetime = this.shadow.querySelector('#datetime');
    }

    events(){
        this.form.addEventListener('submit',this.sentForm.bind(this)); 
    }

    resetValues(){
        this.form.reset();

    }

    sentForm(e){
        let isSent = false;
        e.preventDefault();
        let data = {
            name: this.name.value,
            email: this.email.value,
            description: this.description.value,
            datetime: this.datetime.value
        }
        const url = 'http://localhost:3000/server/reminder/create';
        const http = new XMLHttpRequest();
        http.open("POST", url, true);
        http.setRequestHeader('Content-type', 'application/json')
        http.onload = function(){
            if(this.readyState == 4 && this.status == 200){
                var resultado = JSON.parse(this.responseText)
                alert(resultado.message);
                isSent = true;
            }else{
                var resultado = JSON.parse(this.responseText)
                alert(resultado.message)
            }
        }
        http.send(JSON.stringify(data));
        this.resetValues();
    }

    connectedCallback(){
        this.render();
        this.initVar();
        this.events();
    }

    render() {
        this.shadow.innerHTML = `
            <style>
                .container{
                    padding-right: 15px;
                    padding-left: 15px;
                    margin-right: auto;
                    margin-left: auto;
                }
                
                .container-input {
                    position: relative;
                    display: flex;
                    flex-wrap: wrap;
                    align-items: stretch;
                    width: 100%;
                    margin-top: 10px;
                    margin-bottom: 10px;
                
                }
                
                .form-input{
                    position: relative;
                    flex: 1 1 0%;
                    min-width: 0;
                    margin-bottom: 0;
                    display: block;
                    width: 100%;
                    height: calc(1em + .75rem + 2px);
                    padding: .375rem .75rem;
                }
                
                
                textarea.form-input {
                    height: auto;
                }
                
                .button{
                    display: inline-block;
                    width: 100%;
                    font-weight: 400;
                    padding: .75rem .75rem;
                    font-size: 1rem;
                    text-align: center;
                    vertical-align: middle;
                    font-size: 1rem;
                    line-height: 1.5;
                    border: 1px solid transparent;
                    border-radius: .25rem;
                    user-select: none;
                }
                
                .color-success{
                    color: #fff;
                    background-color: #27AE60;
                    border-color: #27AE60;
                }
                
                
                .color-success:hover{
                    background-color: #259e57;
                    border-color: #259e57;
                }
            </style>

            <div class="container">
                <form enctype="multipart/form-data" method="post" id="formReminder">
                    <div class="container-input">
                        <input class="form-input" placeholder="nombre" type="text" id="name" required >
                    </div>
                    <div class="container-input">
                    <input class="form-input" placeholder="Email" type="email" id="email" required> 
                    </div>
                    <div class="container-input">
                    <textarea class="form-input" cols="10" rows="4" placeholder="Descripcion" id="description" required></textarea> 
                    </div>
                    <div class="container-input">
                    <input class="form-input" type="datetime-local" name="" id="datetime" required> 
                    </div>
                    <div class="container-input">
                    <button class="button color-success" type="submit">Crear recordatorio</button> 
                    </div>
                    
                </form>
            </div>
        `
    }
}

window.customElements.define('form-reminder', FormReminder);