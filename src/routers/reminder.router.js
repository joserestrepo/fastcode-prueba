const { Router } = require('express');

//controller
const { createReminder } = require('./../controllers/reminder.controller');

const router = new Router();

router.post('/create',createReminder);


module.exports = router;