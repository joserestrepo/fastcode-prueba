const app = require('./../server');

const reminderRouter = require('./reminder.router');

app.use('/server/reminder', reminderRouter);