const mongoose = require('mongoose');
const config = require('./config/config');

mongoose.connect(config.db,{
    useUnifiedTopology: true,
    useNewUrlParser: true
}, (err)=>{
    if(err) console.log(err);
    console.log("Connection an Database successfully");
});
