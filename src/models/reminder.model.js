const { mongo } = require("mongoose");

const { Schema, model } = require('mongoose');

const ReminderSchema = new Schema({
    name: {
        type:String,
        require: true
    },
    email: {
        type:String,
        require: true
    },
    description: {
        type:String,
        require: true
    },
    datetime: {
        type:Date,
        require: true
    }
});

module.exports = model('reminder', ReminderSchema);