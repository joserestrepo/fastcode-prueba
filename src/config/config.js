require('dotenv').config();
module.exports = {
    port: process.env.PORT,
    db: process.env.DB,
    //db: process.env.db || 'mongodb+srv://admin:admin@cluster0-pgadm.mongodb.net/test?retryWrites=true&w=majority',
    mail: {
        user: process.env.USER,
        pass: process.env.PASS
    }
}